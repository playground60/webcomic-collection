import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './pages/about/about.component';
import { CategoryEditorComponent } from './pages/collection/category-editor/category-editor.component';
import { CategoryView } from './pages/collection/category-view/category-view.component';
import { CollectionComponent } from './pages/collection/collection.component';
import { SearchComponent } from './pages/collection/search/search.component';
import { WebcomicEditorComponent } from './pages/collection/webcomic-editor/webcomic-editor.component';
import { WebcomicViewComponent } from './pages/collection/webcomic-view/webcomic-view.component';
import { CategoryResolverService } from './services/category-data-resolver.service';
import { WebcomicResolverService } from './services/webomic-data-resolver.service';
import { WebcomicExistsGuard } from './shared/guards/webcomic-exists.guard';

const routes: Routes = [
  { path: '', redirectTo: '/collection', pathMatch: 'full' },
  {
    path: 'collection',
    resolve: [CategoryResolverService, WebcomicResolverService],
    children: [
      { path: '', component: CollectionComponent },
      { path: 'search-results', component: SearchComponent },
      { path: 'new', component: WebcomicEditorComponent },
      { path: 'new-category', component: CategoryEditorComponent },
      { path: 'categories/:name', component: CategoryView, canActivate: [WebcomicExistsGuard] },
      { path: 'categories/:name/edit', component: CategoryEditorComponent, canActivate: [WebcomicExistsGuard] },
      { path: 'categories/:name/:id', component: WebcomicViewComponent, canActivate: [WebcomicExistsGuard] },
      { path: 'webcomics/:id', component: WebcomicViewComponent, canActivate: [WebcomicExistsGuard] },
      { path: 'webcomics/:id/edit', component: WebcomicEditorComponent, canActivate: [WebcomicExistsGuard] },
    ],
  },
  { path: 'about', component: AboutComponent },
  // { path: '**', redirectTo: '/collection', pathMatch: 'full' },
  { path: '**', redirectTo: '/collection' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
