import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent implements OnInit, OnDestroy {
  searchTerm = '';
  searchSubscription: Subscription;

  constructor(private searchService: SearchService, private router: Router) {}

  ngOnInit(): void {
    this.searchSubscription = this.searchService.searchTerm.subscribe(
      (s) => (this.searchTerm = s)
    );
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.resetSearchInput();
      }
    });
  }

  submitSearchRequest() {
    if (this.searchTerm) {
      this.router.navigate(['/collection/search-results'], {
        queryParams: { q: this.searchTerm },
      });
      this.resetSearchInput();
    }
  }

  resetSearchInput() {
    this.searchTerm = '';
    this.syncAllInstances();
  }

  syncAllInstances() {
    this.searchService.setSearchTerm(this.searchTerm);
  }

  ngOnDestroy() {
    this.searchSubscription.unsubscribe();
  }
}
