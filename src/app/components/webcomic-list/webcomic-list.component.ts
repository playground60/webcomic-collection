import { Component, Input, OnChanges } from '@angular/core';
import { Webcomic } from 'src/app/models/webcomic';
import { CategoryService } from 'src/app/services/category.service';
import { WebcomicDataService } from 'src/app/services/webcomic-data.service';

@Component({
  selector: 'app-webcomic-list',
  templateUrl: './webcomic-list.component.html',
  styleUrls: ['./webcomic-list.component.scss'],
})
export class WebcomicListComponent implements OnChanges {
  @Input() webcomics: Webcomic[];
  @Input() categoryName: string;

  emptySpaces = 0;

  constructor(
    private webcomicService: WebcomicDataService,
    private categoryService: CategoryService
  ) {}

  ngOnChanges(): void {
    this.determineSpaces();
  }

  selectWebcomic(webcomic: Webcomic) {
    this.webcomicService.navigateToWebcomic(webcomic, this.categoryName);
  }

  determineCategoryName(webcomic: Webcomic): string {
    let categoryName: string;
    if (this.categoryService.checkCategoryExits) {
      categoryName = webcomic.category;
    } else {
      categoryName = 'Misc';
    }
    return categoryName;
  }

  determineSpaces() {
    if (this.webcomics.length % 3 === 1) {
      this.emptySpaces = 1;
    } else if (this.webcomics.length % 3 === 2) {
      this.emptySpaces = 2;
    }
  }
}
