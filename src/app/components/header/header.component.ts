import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  mobileMenuVisible = false;

  animationEnabled = false;

  constructor() {}

  ngOnInit(): void {}

  enableAnimation() {
    if (this.animationEnabled == false) {
      this.animationEnabled = true;
    }
  }

  toggleMobileMenu() {
    this.mobileMenuVisible = !this.mobileMenuVisible;
  }

  closeMobileMenu() {
    this.mobileMenuVisible = false;
  }

  consumeClickEvent(event: MouseEvent) {
    event.stopPropagation();
  }
}
