import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  mobileSearchVisible = false;

  constructor() {}

  ngOnInit(): void {}

  toggleMobileSearch(event?: MouseEvent) {
    this.mobileSearchVisible = !this.mobileSearchVisible;
    if (event) {
      event.stopPropagation();
    }
  }

  closeMobileSearch() {
    this.mobileSearchVisible = false;
  }
}
