import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import {
  FaIconLibrary,
  FontAwesomeModule,
} from '@fortawesome/angular-fontawesome';
import { faEllipsisH, fas, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { ClickOutsideModule } from 'ng-click-outside';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { WebcomicListComponent } from './components/webcomic-list/webcomic-list.component';
import { AboutComponent } from './pages/about/about.component';
import { CategoryEditorComponent } from './pages/collection/category-editor/category-editor.component';
import { CategoryView } from './pages/collection/category-view/category-view.component';
import { CollectionComponent } from './pages/collection/collection.component';
import { SearchComponent } from './pages/collection/search/search.component';
import { WebcomicEditorComponent } from './pages/collection/webcomic-editor/webcomic-editor.component';
import { WebcomicViewComponent } from './pages/collection/webcomic-view/webcomic-view.component';
import { ImageWithLoadingIndicatorComponent } from './shared/widgets/image-with-loading-indicator/image-with-loading-indicator.component';

@NgModule({
  declarations: [
    AppComponent,
    CollectionComponent,
    HeaderComponent,
    CategoryView,
    CategoryEditorComponent,
    WebcomicViewComponent,
    WebcomicEditorComponent,
    AboutComponent,
    FooterComponent,
    SearchBarComponent,
    WebcomicListComponent,
    SearchComponent,
    ImageWithLoadingIndicatorComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ClickOutsideModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas);
    library.addIcons(faEllipsisH, faSpinner);
  }
}
