import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { CategoryResolverService } from 'src/app/services/category-data-resolver.service';
import { CategoryService } from 'src/app/services/category.service';
import { WebcomicDataService } from 'src/app/services/webcomic-data.service';
import { WebcomicResolverService } from 'src/app/services/webomic-data-resolver.service';

@Injectable({
  providedIn: 'root'
})
export class WebcomicExistsGuard implements CanActivate {

  constructor(
    private webcomicResolver: WebcomicResolverService,
    private categoryResolver: CategoryResolverService,
    private webcomicService: WebcomicDataService,
    private categoryService: CategoryService,
    private router: Router,
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const categoryName = route.params.name;
    const webcomicUuid = route.params.id;
    if (!categoryName && !webcomicUuid) {
      return true;
    }

    return Promise.all([this.webcomicResolver.resolve(route, state), this.categoryResolver.resolve(route, state)]).then(() => {
      let canActivate = true;
      if (categoryName && webcomicUuid) {
        canActivate = this.webcomicService.getFilteredWebcomics(categoryName).some(w => w.uuid === webcomicUuid);
      } else if (categoryName) {
        canActivate = this.categoryService.checkCategoryExits(categoryName);
      } else if (webcomicUuid) {
        canActivate = this.webcomicService.getAllWebcomics().some(w => w.uuid === webcomicUuid);
      }

      if (canActivate) {
        return true;
      } else {
        return this.router.createUrlTree(['/collection']);
      }
    });

  }

}
