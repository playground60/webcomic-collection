import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-image-with-loading-indicator',
  templateUrl: './image-with-loading-indicator.component.html',
  styleUrls: ['./image-with-loading-indicator.component.scss'],
})
export class ImageWithLoadingIndicatorComponent implements OnChanges {
  @Input() imageClass: string;

  @Input() imageUrl: string;
  @Input() altTitle: string;

  isLoading = true;

  constructor() {}

  ngOnChanges() {
    this.isLoading = true;
  }
}
