import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Webcomic } from 'src/app/models/webcomic';
import { WebcomicDataService } from 'src/app/services/webcomic-data.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  searchTerm: string;
  webcomics: Webcomic[];

  get hasResults(): boolean {
    return this.webcomics?.length > 0;
  }

  constructor(
    private webcomicService: WebcomicDataService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((queryParams) => {
      this.searchTerm = queryParams['q'];
      this.webcomics = this.getWebcomicsForSearch();
    });
  }

  private getWebcomicsForSearch(): Webcomic[] {
    const lowerCaseSearchTerm = this.searchTerm.toLowerCase();
    return this.webcomicService.getAllWebcomics()
      .filter((comic) => {
        return comic.tags.some((t) =>
          t.toLowerCase() === lowerCaseSearchTerm ||
          t.split(' ').some((w) => w.toLowerCase() == lowerCaseSearchTerm)
        );
      });
  }
}
