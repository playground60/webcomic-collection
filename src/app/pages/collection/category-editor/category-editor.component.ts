import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from 'src/app/models/category';
import { CategoryService } from 'src/app/services/category.service';
import { urlPattern } from 'src/app/shared/patterns';

@Component({
  selector: 'app-category-editor',
  templateUrl: './category-editor.component.html',
  styleUrls: ['./category-editor.component.scss'],
})
export class CategoryEditorComponent implements OnInit {
  categoryForm: FormGroup;
  category: Category;
  categoryName: string;

  get isNewCategory(): boolean {
    return !this.categoryName;
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private categoryService: CategoryService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => (this.categoryName = params.name));
    if (!this.isNewCategory) {
      this.category = this.categoryService.getCategory(this.categoryName);
    }
    this.initializeForm();
  }

  private initializeForm() {
    this.categoryForm = new FormGroup({
      title: new FormControl(this.category?.name, [
        Validators.required,
        this.validateCategoryIsUnique.bind(this),
      ]),
      url: new FormControl(this.category?.url, [
        Validators.required,
        Validators.pattern(urlPattern),
      ]),
      imageUrl: new FormControl(
        this.category?.imageUrl,
        Validators.pattern(urlPattern)
      ),
      status: new FormControl(this.category?.status),
    });
  }

  saveCategory() {
    if (this.isNewCategory) {
      this.category = new Category(this.categoryForm.value.title, '', '', '');
    }

    this.category.url = this.categoryForm.value.url;
    if (this.categoryForm.value.imageUrl.length > 0) {
      this.category.imageUrl = this.categoryForm.value.imageUrl;
    } else {
      this.category.imageUrl = 'assets/images/misc_2.png';
    }
    this.category.status = this.categoryForm.value.status;

    if (this.isNewCategory) {
      this.categoryService.addNewCategory(this.category);
    } else {
      this.categoryService.updateCategory(this.category);
    }
    this.router.navigate(['/collection/categories/', this.category.name]);
  }

  deleteCategory() {
    this.categoryService.deleteCategory(this.category);
    this.router.navigate(['/collection']);
  }

  validateCategoryIsUnique(control: FormControl): { [s: string]: boolean } {
    if (
      this.isNewCategory &&
      this.categoryService.checkCategoryExits(control.value)
    ) {
      return { noDuplicateCategories: true };
    }
    return null;
  }
}
