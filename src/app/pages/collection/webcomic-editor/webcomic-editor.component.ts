import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from 'src/app/services/category.service';
import { WebcomicDataService } from 'src/app/services/webcomic-data.service';
import { urlPattern } from 'src/app/shared/patterns';
import { createUuid } from 'src/app/shared/uuid';
import { Webcomic } from '../../../models/webcomic';

@Component({
  selector: 'app-webcomic-editor',
  templateUrl: './webcomic-editor.component.html',
  styleUrls: ['./webcomic-editor.component.scss'],
})
export class WebcomicEditorComponent implements OnInit {
  originCategory: string;

  uuid: string;
  webcomic: Webcomic;
  isNewWebcomic: boolean;
  tags: string[];
  categorySuggestions: string[];
  categorySuggestionsVisible = false;

  get exitingsCategories(): string[] {
    return this.categoryService.getCategoryTitles();
  }

  webcomicForm: FormGroup;
  tagForm: FormControl;

  constructor(
    private webcomicService: WebcomicDataService,
    private route: ActivatedRoute,
    private router: Router,
    private categoryService: CategoryService
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((queryParams) => {
      this.originCategory = queryParams.currentCategory;
    });

    this.route.params.subscribe((params) => {
      this.uuid = params['id'];
      this.isNewWebcomic = !this.uuid;

      if (!this.isNewWebcomic) {
        this.webcomic = this.webcomicService.getWebcomicByUuid(this.uuid);
      } else {
        this.webcomic = new Webcomic(createUuid(), '', '', '', '', [], false);
      }
      this.tags = this.webcomic.tags.slice();
      this.initializeForms();
    });
    this.getCategorySuggestions();
  }

  private initializeForms() {
    this.webcomicForm = new FormGroup({
      imageUrl: new FormControl(this.webcomic.imageUrl, [
        Validators.required,
        Validators.pattern(urlPattern),
      ]),
      url: new FormControl(this.webcomic.url, [Validators.pattern(urlPattern)]),
      title: new FormControl(this.webcomic.title),
      category: new FormControl(this.webcomic.category, Validators.required),
    });
    this.tagForm = new FormControl('');
  }

  openCategorySuggestionDropdown(event: MouseEvent) {
    this.categorySuggestionsVisible = true;
    event.stopPropagation();
  }

  closeCategorySuggestionDropdown() {
    this.categorySuggestionsVisible = false;
  }

  getCategorySuggestions() {
    this.categorySuggestions = this.exitingsCategories.filter(
      (c) =>
        c.substr(0, this.webcomicForm.value.category.length).toLowerCase() ===
        this.webcomicForm.value.category.toLowerCase()
    );
  }

  setSuggestedCategory(categoryTitle: string) {
    this.webcomicForm.patchValue({ category: categoryTitle });
    this.categorySuggestionsVisible = false;
  }

  saveWebcomic() {
    this.webcomic.url = this.webcomicForm.value.url;
    this.webcomic.imageUrl = this.webcomicForm.value.imageUrl;
    this.webcomic.title = this.webcomicForm.value.title;
    this.webcomic.category = this.webcomicForm.value.category;
    this.webcomic.tags = this.tags;
    if (this.isNewWebcomic) {
      this.webcomicService.addNewWebcomic(this.webcomic);
    } else {
      this.webcomicService.updateWebcomic(this.webcomic);
    }
    this.webcomicService.navigateToWebcomic(this.webcomic, this.originCategory);
  }

  deleteWebcomic() {
    this.webcomicService.deleteWebcomic(this.webcomic);
    this.router.navigate(['/collection']);
  }

  addTag() {
    this.tags.push(this.tagForm.value);
    this.tagForm.reset();
  }

  deleteTag(index: number) {
    this.tags.splice(index, 1);
  }
}
