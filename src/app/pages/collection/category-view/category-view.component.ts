import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from 'src/app/services/category.service';
import { WebcomicDataService } from 'src/app/services/webcomic-data.service';
import { Category } from '../../../models/category';
import { Webcomic } from '../../../models/webcomic';

@Component({
  selector: 'app-category-view',
  templateUrl: './category-view.component.html',
  styleUrls: ['./category-view.component.scss'],
})
export class CategoryView implements OnInit {
  categoryName: string;
  category: Category;
  webcomics: Webcomic[];
  categoryEditable: boolean;
  categoryMenuVisible = false;

  constructor(
    private route: ActivatedRoute,
    private categoryService: CategoryService,
    private webcomicService: WebcomicDataService
  ) {}

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.route.params.subscribe((params) => {
      this.categoryName = params['name'];
      this.category = this.categoryService.getCategory(this.categoryName);
      this.webcomics = this.webcomicService.getFilteredWebcomics(
        this.category.name
      );
    });
  }

  openWebcomicSource() {
    window.open(this.category.url, '_blank');
  }

  toggleCategoryMenu(event: MouseEvent) {
    this.categoryMenuVisible = !this.categoryMenuVisible;
    event.stopPropagation();
  }

  closeCategoryMenu() {
    this.categoryMenuVisible = false;
  }
}
