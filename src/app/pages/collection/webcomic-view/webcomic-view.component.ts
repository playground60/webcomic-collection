import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Webcomic } from 'src/app/models/webcomic';
import { CategoryService } from 'src/app/services/category.service';
import { WebcomicDataService } from 'src/app/services/webcomic-data.service';

@Component({
  selector: 'app-webcomic-view',
  templateUrl: './webcomic-view.component.html',
  styleUrls: ['./webcomic-view.component.scss'],
})
export class WebcomicViewComponent implements OnInit {
  webcomic: Webcomic;
  webcomicTitle: string;
  webcomicCategory: string;
  categoryImageUrl: string;
  webcomics: Webcomic[];

  showZoomImage = false;

  constructor(
    private webcomicService: WebcomicDataService,
    private categoryService: CategoryService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.route.params.subscribe((params: Params) => {
      this.webcomic = this.webcomicService.getWebcomicByUuid(params.id);
      this.webcomicCategory = params.name;
      this.getSubheaderData();
      this.getWebcomics();
    });
  }

  private getWebcomics() {
    if (!this.webcomicCategory) {
      this.webcomics = this.webcomicService.getAllWebcomics();
    } else {
      this.webcomics = this.webcomicService.getFilteredWebcomics(
        this.webcomicCategory
      );
    }
  }

  private getSubheaderData() {
    this.webcomicTitle = this.getWebcomicTitle(this.webcomic);
    this.categoryImageUrl = this.getCategoryImageUrl();
  }

  goToWebcomicCategory() {
    if (this.categoryService.checkCategoryExits(this.webcomic.category)) {
      this.router.navigate(['/collection/categories', this.webcomic.category]);
    } else {
      this.router.navigate(['/collection/categories/misc']);
    }
  }

  navigate(direction: string) {
    this.webcomicService.navigateToWebcomic(
      this.webcomics[this.getIndexForNavigation(direction)],
      this.webcomicCategory
    );
  }

  private getIndexForNavigation(direction: string): number {
    let recentIndex = this.webcomics.findIndex(
      (w) => w.uuid === this.webcomic.uuid
    );
    let nextIndex: number;
    if (direction === 'next') {
      if (recentIndex < this.webcomics.length - 1) {
        nextIndex = recentIndex + 1;
      } else {
        nextIndex = 0;
      }
    } else if (direction === 'previous') {
      if (recentIndex === 0) {
        nextIndex = this.webcomics.length - 1;
      } else {
        nextIndex = recentIndex - 1;
      }
    }
    return nextIndex;
  }

  private getWebcomicTitle(webcomic: Webcomic): string {
    let webcomicTitle: string;
    if (webcomic.title.length > 0) {
      webcomicTitle = webcomic.title;
    } else {
      const webcomicNumber =
        this.webcomicService
          .getFilteredWebcomics(webcomic.category)
          .findIndex((w) => w.uuid === webcomic.uuid) + 1;
      webcomicTitle = `${this.webcomic.category} comic No. ${webcomicNumber}`;
    }
    return webcomicTitle;
  }

  private getCategoryImageUrl(): string {
    let categoryImageUrl: string;
    if (this.categoryService.checkCategoryExits(this.webcomic.category)) {
      categoryImageUrl = this.categoryService.getCategory(
        this.webcomic.category
      ).imageUrl;
    } else {
      categoryImageUrl = 'assets/images/misc_2.png';
    }
    return categoryImageUrl;
  }

  toggleImageZoom() {
    this.showZoomImage = !this.showZoomImage;
  }

  like() {
    this.webcomic.like = !this.webcomic.like;
    this.webcomicService.updateWebcomic(this.webcomic);
  }

  openWebcomicSource() {
    if (this.webcomic.url.length > 0) {
      window.open(this.webcomic.url, '_blank');
    }
  }

  goToWebcomicEdit() {
    this.router.navigate(
      ['collection/webcomics/', this.webcomic.uuid, 'edit'],
      {
        queryParams: { currentCategory: this.webcomicCategory },
      }
    );
  }

  saveURLToClipboard() {
    this.copyToClipboard(this.webcomic.imageUrl);
  }

  private copyToClipboard(str: string) {
    let element = document.createElement('textarea');
    element.value = str;
    document.body.appendChild(element);
    element.select();
    document.execCommand('copy');
    document.body.removeChild(element);
  }
}
