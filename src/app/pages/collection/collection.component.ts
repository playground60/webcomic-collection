import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Webcomic } from 'src/app/models/webcomic';
import { CategoryService } from 'src/app/services/category.service';
import { WebcomicDataService } from 'src/app/services/webcomic-data.service';
import { Category } from '../../models/category';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss'],
})
export class CollectionComponent implements OnInit {
  categories: Category[];
  selectedCategory: Category;
  showAllWebcomics = false;
  emptySpaces = 0;
  webcomics: Webcomic[];

  constructor(
    private router: Router,
    private categoryService: CategoryService,
    private webcomicService: WebcomicDataService
  ) {}

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.categories = this.categoryService.getCategories();
    this.webcomics = this.webcomicService.getAllWebcomics();
    this.determineSpaces();
  }

  selectCategory(category: Category) {
    this.selectedCategory = category;
    this.router.navigate([
      '/collection/categories',
      this.selectedCategory.name,
    ]);
  }

  showWebcomics() {
    this.showAllWebcomics = true;
  }

  showCategories() {
    this.showAllWebcomics = false;
    window.scrollTo(0, 0);
  }

  determineSpaces() {
    if (this.categories.length % 3 === 1) {
      this.emptySpaces = 1;
    } else if (this.categories.length % 3 === 2) {
      this.emptySpaces = 2;
    }
  }
}
