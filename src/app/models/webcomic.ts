export class Webcomic {
  public readonly uuid: string;
  public url: string;
  public imageUrl: string;
  public title: string;
  public category: string;
  public tags: string[];
  public like: boolean;

  constructor(
    uuid: string,
    url: string,
    imageUrl: string,
    title: string,
    category: string,
    tags: string[],
    like: boolean
  ) {
    this.uuid = uuid;
    this.url = url;
    this.imageUrl = imageUrl;
    this.title = title;
    this.category = category;
    this.tags = tags;
    this.like = like;
  }
}
