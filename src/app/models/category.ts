export class Category {
  public readonly name: string;
  public url: string;
  public imageUrl: string;
  public status: string;

  constructor(name: string, url: string, imageUrl: string, status: string) {
    this.name = name;
    this.url = url;
    this.imageUrl = imageUrl;
    this.status = status;
  }
}
