import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SearchService {
  private searchTermSubject = new BehaviorSubject<string>('');

  get searchTerm(): Observable<string> {
    return this.searchTermSubject.asObservable();
  }

  setSearchTerm(term: string) {
    this.searchTermSubject.next(term);
  }
}
