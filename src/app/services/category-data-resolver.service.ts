import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { Category } from '../models/category';
import { CategoryService } from './category.service';
import { WebStorageService } from './web-storage.service';

@Injectable({
  providedIn: 'root',
})
export class CategoryResolverService implements Resolve<Category[]> {
  constructor(
    private webService: WebStorageService,
    private categoryService: CategoryService
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Category[]> | Promise<Category[]> | Category[] {
    const categories = this.categoryService.getCategories();
    if (categories.length > 0) {
      return categories;
    } else {
      return this.webService.loadCategories().then((categories) => {
        this.categoryService.setCategories(categories);
        return categories;
      });
    }
  }
}
