import { Injectable } from '@angular/core';
import { Category } from '../models/category';
import { WebStorageService } from './web-storage.service';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  private categories: Category[] = [];

  constructor(private webService: WebStorageService) {}

  getCategories(): Category[] {
    return this.categories.slice();
  }

  setCategories(categories: Category[]) {
    this.categories = categories;
  }

  getCategory(name: string): Category {
    if (!name) {
      return null;
    }
    return this.categories.find(
      (c) => c.name.toLowerCase() === name.toLowerCase()
    );
  }

  checkCategoryExits(name: string): boolean {
    if (!name) {
      return false;
    }
    return this.categories.some(
      (c) => c.name.toLowerCase() === name.toLowerCase()
    );
  }

  getCategoryTitles(): string[] {
    return this.categories
      .filter((c) => c.name !== 'Favorites' && c.name !== 'Misc')
      .map((c) => c.name);
  }

  addNewCategory(category: Category) {
    this.categories.push(category);
    this.webService.storeCategories(this.categories);
  }

  updateCategory(category: Category) {
    const index = this.categories.findIndex(
      (c) => c.name.toLowerCase() === category.name.toLowerCase()
    );
    this.categories[index] = category;
    this.webService.storeCategories(this.categories);
  }

  deleteCategory(category: Category) {
    const index = this.categories.findIndex(
      (c) => c.name.toLowerCase() === category.name.toLowerCase()
    );
    this.categories.splice(index, 1);
    this.webService.storeCategories(this.categories);
  }
}
