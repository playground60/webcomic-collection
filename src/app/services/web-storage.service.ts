import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Category } from '../models/category';
import { Webcomic } from '../models/webcomic';

@Injectable({
  providedIn: 'root',
})
export class WebStorageService {
  constructor(private http: HttpClient) {}

  storeWebcomics(webcomics: Webcomic[]) {
    this.http
      .put(
        `${environment.firebaseUri}//${environment.webcomicsEndpoint}`,
        webcomics
      )
      .toPromise();
  }

  storeCategories(categories: Category[]) {
    this.http
      .put(
        `${environment.firebaseUri}//${environment.categoriesEndpoint}`,
        categories
      )
      .toPromise();
  }

  loadWebcomics(): Promise<Webcomic[]> {
    return this.http
      .get<Webcomic[]>(
        `${environment.firebaseUri}//${environment.webcomicsEndpoint}`
      )
      .toPromise()
      .then((webcomics) => {
        webcomics.forEach((w) => (w.tags = w.tags || []));
        return webcomics;
      });
  }

  loadCategories(): Promise<Category[]> {
    return this.http
      .get<Category[]>(
        `${environment.firebaseUri}//${environment.categoriesEndpoint}`
      )
      .toPromise();
  }
}
