import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Webcomic } from '../models/webcomic';
import { CategoryService } from './category.service';
import { WebStorageService } from './web-storage.service';

@Injectable({
  providedIn: 'root',
})
export class WebcomicDataService {
  private webcomics: Webcomic[] = [];

  constructor(
    private router: Router,
    private categoryService: CategoryService,
    private webService: WebStorageService
  ) {}

  setWebcomics(webcomics: Webcomic[]) {
    this.webcomics = webcomics;
  }

  getAllWebcomics(): Webcomic[] {
    return this.webcomics.slice();
  }

  getFilteredWebcomics(categoryName: string): Webcomic[] {
    let filteredWebcomics: Webcomic[];
    if (categoryName === 'Favorites') {
      filteredWebcomics = this.getFavorites();
    } else if (categoryName === 'Misc') {
      filteredWebcomics = this.getMiscWebcomics();
    } else {
      filteredWebcomics = this.webcomics.filter((w) =>
        w.category.toLowerCase().includes(categoryName.toLowerCase())
      );
    }
    return filteredWebcomics;
  }

  getMiscWebcomics(): Webcomic[] {
    return this.webcomics.filter(
      (w) => !this.categoryService.checkCategoryExits(w.category)
    );
  }

  getFavorites(): Webcomic[] {
    return this.webcomics.filter((webcomic) => webcomic.like);
  }

  getWebcomicByUuid(uuid: string): Webcomic {
    return this.webcomics.find((w) => w.uuid === uuid);
  }

  navigateToWebcomic(webcomic: Webcomic, categoryName: string) {
    let urlBase: string;
    if (categoryName) {
      urlBase = `/collection/categories/${categoryName}`;
    } else {
      urlBase = '/collection/webcomics';
    }
    this.router.navigate([urlBase, webcomic.uuid]);
  }

  addNewWebcomic(webcomic: Webcomic) {
    this.webcomics.push(webcomic);
    this.webService.storeWebcomics(this.webcomics);
  }

  updateWebcomic(webcomic: Webcomic) {
    const index = this.webcomics.findIndex((w) => w.uuid === webcomic.uuid);
    this.webcomics[index] = webcomic;
    this.webService.storeWebcomics(this.webcomics);
  }

  deleteWebcomic(webcomic: Webcomic) {
    const index = this.webcomics.findIndex((w) => w.uuid === webcomic.uuid);
    this.webcomics.splice(index, 1);
    this.webService.storeWebcomics(this.webcomics);
  }
}
