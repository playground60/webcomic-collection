import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { Webcomic } from '../models/webcomic';
import { WebStorageService } from './web-storage.service';
import { WebcomicDataService } from './webcomic-data.service';

@Injectable({
  providedIn: 'root',
})
export class WebcomicResolverService implements Resolve<Webcomic[]> {
  constructor(
    private webService: WebStorageService,
    private webcomicService: WebcomicDataService
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Webcomic[]> | Promise<Webcomic[]> | Webcomic[] {
    const webcomics = this.webcomicService.getAllWebcomics();
    if (webcomics.length > 0) {
      return Promise.resolve(webcomics);
    } else {
      return this.webService.loadWebcomics().then((webcomics) => {
        this.webcomicService.setWebcomics(webcomics);
        return webcomics;
      });
    }
  }
}
