# Webcomic Collector

You can find  a demo of Webcomic collector under https://webcomic.hensler.dev

## About

Webcomic collector is an ever-growing, always-under-construction practice pet project of mine. When I began playing around with web development, I needed to come up with an idea easy enough to start out with but also flexible and interesting enough to expand over time into something more complex. I knew I didn't want to make a 8904850th recipe collection, shopping list, notice board or similar overused beginner project but something I care about and could actually use. Around the same time I wanted to send a friend the URL of a picture from an old webcomic from about a decade ago, but I only vaguely remembered the webcomic's topic, not to mention it's title. After a mere 2 hour search it dawned on me, perhaps there was a better way of organizing webcomics than either having multiple 'Webcomic' bookmark folders scattered around on multiple devices or, my go to method lately, just trying to play mind palace. This is how the idea of webcomic collector was born. An app to collect and and organize webcomic URLs with image previews and share functionality. Of course there is also the possibility to search the webcomic collection by some vague memory with the help of custom tags. While it is is still in development the basic functionality is already in place. Next steps will involve general cleanup, building a scraper to extract image URLs from the webcomic's URL and to expand the share functionaly beyond copying the image URL to clipboard.

## Getting started
Clone this repository.  
Install the required dependecies with `npm install`.  
Execute the app with `npm run start`.  
Open http://localhost:4200 in your browser.




 